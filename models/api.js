var app = require('./express.js');
var Data = require('./data.js');
var User = require('./user.js');
var algorithm = require('./algorithm');
var mongoose = require('mongoose');
var findOrCreate = require('mongoose-findorcreate')
// setup body parser
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// submit survey answers
app.post('/api/data', function (req,res) {
  Data.find({token:req.body.token}, function(err,data) {
	if (err) {
	  res.sendStatus(403);
	  return;
	}
	if (data.length == 0) {
		Data.create({token:req.body.token,ratings:req.body.ratings,preferences:req.body.preferences}, function(err,response){
			res.json({response:response});
		});
	}
	if (data.length == 1) {
		Data.update({token:req.body.token}, {ratings:req.body.ratings,preferences:req.body.preferences}, function(err,response){
			res.json({response:response});
	});
	}
  });
});

// get phone results
app.get('/api/results', function(req, res) {
    console.log(req.headers.authorization)
	Data.find({token:req.headers.authorization},function(err,data)
	{
        console.log(data);
	 	algorithm.run(data[0].ratings,data[0].preferences,function(results)
	 	{
	 		res.json({result0:results[0],result1:results[1],result2:results[2]})
	 	});
	});
});

// USER AUTHENTICATION METHODS

// register new user
app.post('/api/register', function(req, res) {
    // find or create user with given username
    User.findOrCreate({username: req.body.username}, function(err, user, created) {
        if (created) {
            // if username is not taken, create user record
            user.name = req.body.name;
            user.set_password(req.body.password);
            user.save(function(err) {
                if (err) {
                    res.sendStatus(403);
                    return;
                }
                // create token
                var token = User.generateToken(user.username);
                // return value is JSON containing user's name and token
                res.json({name: user.name, token: token});
            });
        }
        else {
            // return error if username is taken
            res.sendStatus(403);
        }
    });
});

// login existing user
app.post('/api/login', function(req, res) {
    // find user with given username
    User.findOne({username: req.body.username}, function(err, user) {
        if (err) {
            res.sendStatus(403);
            return;
        }
        // check that the user exists and password is correct
        if (user && user.checkPassword(req.body.password)) {
            // create token
            var token = User.generateToken(user.username);
            // return value is JSON containing user's name and token
            res.json({name: user.name, token: token});
        }
        else {
            res.sendStatus(403);
        }
    });
});
