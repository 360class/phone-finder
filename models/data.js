var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var findOrCreate = require('mongoose-findorcreate')



var dataSchema = new Schema({
 token:String,
 ratings:[String],
 preferences:[String]
});

// ensure schemas use virtual IDs
dataSchema.set('toJSON', {
  virtuals: true
});

// add findorCreate
dataSchema.plugin(findOrCreate);

// create item
var Data = mongoose.model('data', dataSchema);

module.exports = Data;
