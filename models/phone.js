var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var findOrCreate = require('mongoose-findorcreate')



var phoneSchema = new Schema({
  name: String,
  operatingsystem: String,
  brand: String, 
  visualappeal: [String],
  price: String,
  screensize: String,
  reliability: String,
  speed: String,
  batterylife: String,
  picture: String,
  link: String,
});

phoneSchema.static('findbyfield',function(findstring,value, cb)
{
  switch(findstring)
  {
    case 'operatingsystem': return this.find({'operatingsystem':value},cb);
          break;
    case 'brand': return this.find({'brand':value},cb);
          break;
    case 'screensize': return this.find({'screensize':value},cb);
          break;
    case 'price': return this.find({'price':value},cb);
          break;
    case 'reliability': return this.find({'reliability':value},cb);
          break;
    case 'speed': return this.find({'speed':value},cb);
          break;
    case 'batterylife': return this.find({'batterylife':value},cb);
          break;
    case 'visualappeal': return this.find({'visualappeal':value},cb);
          break;
  }
});
// ensure schemas use virtual IDs
phoneSchema.set('toJSON', {
  virtuals: true
});

// add findorCreate
phoneSchema.plugin(findOrCreate);

// create item
var Phone = mongoose.model('phones', phoneSchema);

module.exports = Phone;
