var Router = ReactRouter.Router;
var Link = ReactRouter.Link;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;
var Alert = ReactBootstrap.Alert;
var Input = ReactBootstrap.Input;

var App = React.createClass({

    getInitialState: function() {
        return {
            loggedIn: app_api.userLoggedIn()
        };
    },
    setStateonLogin: function(loggedIn)
    {
    	this.state.loggedIn = loggedIn;
    },
    componentWillMount: function()
    {
    	app_api.onChange = this.setStateonLogin

    },
    logout: function()
    {
    	console.log("boooo")
    	app_api.logout()
    },
	render: function() {
	return (
		<div>
		<nav className="navbar navbar-default" role="navigation">
			<div className="container">
				<div className="navbar-header">
				 <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					 <span className="sr-only">Toggle navigation</span>
					 <span className="icon-bar"></span>
					 <span className="icon-bar"></span>
					 <span className="icon-bar"></span>
				</button>
				<a className="navbar-brand" href="/">Home</a>
				</div>
				<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                {this.state.loggedIn ? (
				    <ul className="nav navbar-nav">
					    <li><Link to="/" onClick={this.logout}>Logout</Link></li>
					    <li><div className = "navbar-brand"></div></li>
				    </ul>
			    ) : (
				    <ul className="nav navbar-nav">
					    <li><Link to="login">Login</Link></li>
					    <li><Link to="register">Register</Link></li>
				    </ul>
				)}
				</div>
			</div>
		</nav>
		<div className="container">
			{this.props.children}
		</div>
		</div>
	);
	}
});
var Home = React.createClass({
	render: function() {
	if(app_api.userLoggedIn())
	{	
		return (
			<div className="container">
				<div className="vertical-center-row" style={{width:"200%"}}>
					<div className="text-center col-md-4 col-md-offset-1"> 
						<h1>Home</h1>
						<img src="http://cdn.mos.techradar.com/art/mobile_phones/Best%20Phones%20in%20the%20World/BestPhones-Mar15-970-80.jpg" style={{overflow:'auto',width:"100%"}}/>
						<p>Having a hard time deciding which phone is the best phone for you? Just create an account and Click "Go!" to help make this decision much easier for you!</p>
						<Link to="survey"><button className="btn btn-primary">Go!</button></Link>
					</div>
				</div>
			</div>
		)
	}
	if(!app_api.userLoggedIn())
	{
		return(
			<div className="container">
			<div className="vertical-center-row" style={{width:"200%"}}>
				<div className="text-center col-md-4 col-md-offset-1"> 
					<h1>Home</h1>
					<img src="http://www.stallionpalace.com/wp-content/uploads/2015/03/best-mobile-phones-2015-EshoWorld.jpg" style={{overflow:'auto',width:"100%"}}/>
					<p>Having a hard time deciding which phone is the best phone for you? Just create an account and Click "Go!" to help make this decision much easier for you!</p>
					<Link to="login"><button className="btn btn-primary">Login!</button>  </Link>
					<Link to="register"><button className="btn btn-primary">Register!</button></Link>
				</div>
			</div>
		</div>
		)
	}	
}
});
var Results = React.createClass({
	getInitialState: function(){
		return {results:null};
	},
	componentDidMount: function()
	{
		var self = this;
		app_api.getResults(function(res)
		{
			self.setState({
				results:res
			})
		});
	},
	render: function(){
		console.log(this.state.results);
		if(this.state.results == null)
		{
			return (
				<div className="vertical-center-row" style={{width:"100%"}}>
					<div className="text-center col-md-4 col-md-offset-1"> 
						<h1>Results</h1>
						<p>put your results here</p>
					</div>
				</div>
			);
		}
		if(this.state.results != null)
		{
			return (
				<div className="vertical-center-row" style={{width:"100%"}}>
					<div className="text-center col-md-4"> 
						<h1>Results</h1>
						<p>Phone suggestion 1: {this.state.results.result0.name}</p>
						<img src= {this.state.results.result0.picture} style={{width:"100%"},{height:"250px"}}/>
						<p><a href= {this.state.results.result0.link}>Reviews</a></p>
					</div>
					<div className="text-center col-md-4"> 
						<h1>Results</h1>
						<p>Phone suggestion 2: {this.state.results.result1.name} </p>
						<img src= {this.state.results.result1.picture} style={{width:"100%"},{height:"250px"}}/>
						<p><a href= {this.state.results.result1.link}>Reviews</a></p>
					</div>
					<div className="text-center col-md-4">
						<h1>Results</h1> 
						<p>Phone suggestion 3: {this.state.results.result2.name}</p>
						<img src= {this.state.results.result2.picture} style={{width:"100%"},{height:"250px"}}/>
						<p><a href= {this.state.results.result2.link}>Reviews</a></p>
				 	</div>
 				</div>
			);
		}
	}
});
var DoublePage = React.createClass({
	getInitialState: function()
	{

		return {submit:false,message:"",ratings:[],preferences:[]};
	},
	handleClick: function(event)
	{
		if(this.state.submit == false)
		{
			this.state.ratings =  this.refs.page.submitdata();
			if(this.hasDuplicates(this.state.ratings))
			{
				this.setState({message: "duplicate answer, please fix"})
			}
			else
			{
				this.setState({submit: !this.state.submit});
			}
		}
		if(this.state.submit == true)
		{
			this.state.preferences = this.refs.survey.submitdata();
			app_api.addSurvey(localStorage.token,this.state.ratings,this.state.preferences,function(success,res)
			{
			});
		}
	},
	hasDuplicates: function(array)
	{
		return(new Set(array)).size !== array.length;
	},
	render: function(){
		if(this.state.submit == false)
		{
			return(
				<div className="container">
					<div className="vertical-center-row">
						<div className="text-center col-md-4 col-md-offset-4">
							<Page ref = "page" />
						<div>{this.state.message}</div>
							<div className="btn btn-primary" onClick={this.handleClick}>Submit!</div>
						</div>
					</div>
				</div>
			);
		}
		if(this.state.submit == true)
		{
			console.log("hey there")
			return(
				<div className="container">
					<div className="vertical-center-row">
						<div className="text-center col-md-4 col-md-offset-4">
							<Survey ref = "survey"/>
							<Link to="results"><button className="btn btn-primary" onClick={this.handleClick}>Submit!</button></Link>
						</div>
					</div>
				</div>
			);
		}	
	}
});
var Page = React.createClass({
	submitdata: function(){
		var results = [];
		results[0] = this.refs.operatingsystem.showValue()
		results[1] = this.refs.brand.showValue()
		results[2] = this.refs.visualappeal.showValue()
		results[3] = this.refs.price.showValue()
		results[4] = this.refs.screensize.showValue()
		results[5] = this.refs.reliability.showValue()
		results[6] = this.refs.speed.showValue()
		results[7] = this.refs.batterylife.showValue()
		return results;
	},
	render: function() {
	return (
		<div>
			<h1>Page</h1>
			<p>Put the first survey here</p>
			<p><strong>Help us understand which features are most important to you by ranking these features from 1 to 8 (1 being most important)</strong></p>
			<div className="form-group">
				<p>OperatingSystem:</p>
				<RateSurvey ref="operatingsystem" />
				<p>Brand:</p>
				<RateSurvey ref="brand" />
				<p>Visual Appeal:</p>
				<RateSurvey ref="visualappeal" />
				<p>Price:</p>
				<RateSurvey ref="price" />
				<p>Screen-size:</p>
				<RateSurvey ref="screensize" />
				<p>Reliability:</p>
				<RateSurvey ref="reliability" />
				<p>Speed:</p>
				<RateSurvey ref="speed" />
				<p>Battery Life:</p>
				<RateSurvey ref="batterylife" />
			</div>
		</div>
	);
	}
});
var Survey = React.createClass({
	submitdata: function(){
		var results = []; 
		results[0] = this.refs.OS.showValue()
		results[1] = this.refs.brand.showValue()
		results[2] = this.refs.visualappeal.showValue()
		results[3] = this.refs.price.showValue()
		results[4] = this.refs.screensize.showValue()
		results[5] = this.refs.reliability.showValue() 
		results[6] = this.refs.speed.showValue() 
		results[7] = this.refs.batterylife.showValue()
		return results;
	},
	render: function(){
		console.log("hey baby");
		return(
			<div>
				<h1>Survey</h1>
				<p>Put the final survey here</p>
				<div className="form-group">
					<OperatingSystem ref="OS"/>
					<p/>
					<Brand ref="brand" />
					<p/>
					<VisualAppeal ref="visualappeal" />
					<p/>
					<Price ref="price" />
					<p/>
					<ScreenSize ref="screensize" />
					<p/>
					<Reliability ref="reliability" />
					<p/>
					<Speed ref="speed"/>
					<p/>
					<BatteryLife ref="batterylife" />
					<p/>
				</div>
			</div>
		);
	}
});
var OperatingSystem = React.createClass({
    getInitialState:function(){
      return {selectValue:'IOS'};
    },
    handleChange:function(e){
    this.setState({selectValue:e.target.value});
    },
    showValue: function(){
		return this.state.selectValue
    },
    render: function() {
    var message='You selected '+this.state.selectValue;
    return (
      <div>
        <p>Which Operating System do you prefer?</p>
        <select value={this.state.selectValue} onChange={this.handleChange}>
          <option value="IOS">IOS</option>
          <option value="Android">Android</option>
          <option value="Windows">Windows</option>
        </select>
      </div>        
    );
  }
});
var Brand = React.createClass({
	getInitialState:function(){
      return {selectValue:'HTC'};
    },
    handleChange:function(e){
    this.setState({selectValue:e.target.value});
    },
    showValue: function(){
		return this.state.selectValue
    },
	render: function() {
    var message='You selected '+this.state.selectValue;
    return (
      <div>
        <p>Which brand do you prefer?</p>
        <select value={this.state.selectValue} onChange={this.handleChange}>
		  <option value="HTC">HTC</option>
          <option value="LG">LG</option>
          <option value="Samsung">Samsung</option>
          <option value="Apple">Apple</option>
          <option value="Other">Other</option>
        </select>
      </div>        
    );
  }
});
var VisualAppeal = React.createClass({
	getInitialState:function(){
		return {selectValue:'Silver'};
    },
    handleChange:function(e){
		this.setState({selectValue:e.target.value});
    },
    showValue: function(){
		return this.state.selectValue
    },
	render: function(){
	var message='You selected '+this.state.selectValue;
	return(
	  <div>
	    <p>Which color do you prefer?</p>
	    <select value={this.state.selectValue} onChange={this.handleChange}>
	      <option value = "Silver">Silver</option>
		  <option value = "Black">Black</option>
		  <option value = "Gray">Gray</option>
		  <option value = "White">White</option>
		  <option value = "Gold">Gold</option>
		  <option value = "Other">Other</option>
     	</select>
	  </div>
	);
  }
});
var Price = React.createClass({
	getInitialState:function(){
		return {selectValue:'$100-200'};
    },
    handleChange:function(e){
		this.setState({selectValue:e.target.value});
    },
    showValue: function(){
		return this.state.selectValue
    },
	render: function(){
	var message='You selected '+this.state.selectValue;
	return(
	  <div>
	    <p>What is your price range?(prices are including 2 year contract)</p>
		<select value={this.state.selectValue} onChange={this.handleChange}>
		  <option value = "$100-200">$100-200</option>
		  <option value = "$200-300">$200-300</option>
		  <option value = "$300-400">$300-400</option>
		  <option value = "$400-500">$400-500</option>
		</select>
	  </div>
	);
  }
});
var ScreenSize = React.createClass({
	getInitialState:function(){
		return {selectValue:'Small'};
    },
    handleChange:function(e){
		this.setState({selectValue:e.target.value});
	},
	showValue: function(){
		return this.state.selectValue
    },
	render: function(){
	var message='You selected '+this.state.selectValue;
	return(
      <div>
        <p>What size of screen do you prefer?</p>
	    <select value={this.state.selectValue} onChange={this.handleChange}>
		  <option value = "Small">Small</option>
		  <option value = "Medium">Medium</option>
		  <option value = "Large">Large</option>
		</select>
	  </div>
	);
  }
});
var Reliability = React.createClass({
	getInitialState:function(){
		return {selectValue:'Like an eggshell'};
	},
	handleChange:function(e){
		this.setState({selectValue:e.target.value});
	},
	showValue: function(){
		return this.state.selectValue
    },
	render: function(){
	var message='You selected '+this.state.selectValue;
	return(
	  <div>
	    <p>How strong do you want your phone?</p>
		<select value={this.state.selectValue} onChange={this.handleChange}>
			<option value = "Like an eggshell">Like an eggshell</option>
			<option value = "Drop it a couple times">Drop it a couple times</option>
			<option value = "Doubles as an anvil">Doubles as an anvil</option>
		</select>
   	  </div>
	);
  }
});
var Speed = React.createClass({
	getInitialState:function(){
		return {selectValue:'Average'};
	},
	handleChange:function(e){
		this.setState({selectValue:e.target.value});
	},
	showValue: function(){
		return this.state.selectValue
    },
	render: function(){
	var message='You selected '+this.state.selectValue;
	return(
	  <div>
	    <p>How fast do you want your phone?</p>
		<select value={this.state.selectValue} onChange={this.handleChange}>
	      <option value = "Average">Average</option>
		  <option value = "Quick">Quick</option>
		  <option value = "Lightning Fast">Lightning Fast</option>
		</select>
	  </div>
	);
  }
});
var BatteryLife = React.createClass({
	getInitialState:function(){
		return {selectValue:'Short'};
	},
	handleChange:function(e){
		this.setState({selectValue:e.target.value});
	},
	showValue: function(){
		return this.state.selectValue
    },
	render: function(){
	var message='You selected '+this.state.selectValue;
	return(
	  <div>
	    <p>How long do you want your phone to last?</p>
		<select value={this.state.selectValue} onChange={this.handleChange}>
	      <option value = "Short">Short</option>
		  <option value = "Medium">Medium</option>
		  <option value = "Long">Long</option>
		</select>
	  </div>
	);
  }
});
var RateSurvey = React.createClass({
    getInitialState:function(){
      return {selectValue:'1'};
  },
    handleChange:function(e){
    this.setState({selectValue:e.target.value});
  },
  showValue: function(){
		return this.state.selectValue
  },
  render: function() {
    return (
      <div>
      <select 
        value={this.state.selectValue} 
        onChange={this.handleChange}
      >
       <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>
        <option value="8">8</option>
      </select>
      </div>        
    );
  }
});
var NumberInput = React.createClass({
	getInitialState:function() {
	return {value: ''};
	},

	validationState: function() {
	let num = this.state.value;
	if (num <=10 && num >=1) return 'success';
	else return 'error';
	},
	handleChange: function() {
 
	this.setState({
		value: this.refs.input.getValue()
	});
	},
	render: function() {
	return (
		<Input
		type="text"
		value={this.state.value}
		placeholder="Enter a number between 1-10"
		bsStyle={this.validationState()}
		hasFeedback
		ref="input"
		groupClassName="group-class"
		labelClassName="label-class"
		onChange={this.handleChange} />
	);
	}
});

var History = ReactRouter.History;

// Login page, shows the login form and redirects to the home page if login is successful
var Login = React.createClass({
  // mixin for navigation
  mixins: [ History ],

  // initial state
  getInitialState: function() {
    return {
      // there was an error on logging in
      error: false
    };

  },

  // handle login button submit
  login: function(event) {
    // prevent default browser submit
    event.preventDefault();
    // get data from form
    var username = this.refs.username.value;
    var password = this.refs.password.value;
    if (!username || !password) {
      return;
    }
    // login via API
    app_api.login(username, password, function(loggedIn) {
      // login callback
      if (!loggedIn)
        return this.setState({
          error: true
        });
      this.history.pushState(null, '/');
    }.bind(this));
  },

  // show the login form
  render: function() {
    return (
      <div>
        <h2>Login</h2>
        <form className="form-vertical" onSubmit={this.login}>
          <input type="text" placeholder="Username" ref="username" autoFocus={true} />
          <input type="password" placeholder="Password" ref="password"/>
          <input className="btn btn-warning" type="submit" value="Login" />
          {this.state.error ? (
             <div className="alert">Invalid username or password.</div>
           ) : null}
        </form>
      </div>
    );
  }
});

// Register page, shows the registration form and redirects to the home page if login is successful
var Register = React.createClass({
  // mixin for navigation
  mixins: [ History ],

  // initial state
  getInitialState: function() {
    return {
      // there was an error registering
      error: false
    };
  },

  // handle register button submit
  register: function(event) {
    // prevent default browser submit
    event.preventDefault();
    // get data from form
    var name = this.refs.name.value;
    var username = this.refs.username.value;
    var password = this.refs.password.value;
    if (!name || !username || !password) {
      return;
    }
    // register via the API
    app_api.register(name, username, password, function(loggedIn) {
      // register callback
      if (!loggedIn)
        return this.setState({
          error: true
        });
      this.history.pushState(null, '/');
    }.bind(this));
  },

  // show the registration form
  render: function() {
    return (
      <div>
        <h2>Register</h2>
        <form onSubmit={this.register}>
          <input type="text" placeholder="Name" ref="name" autoFocus={true} />
          <input type="text" placeholder="Username" ref="username"/>
          <input type="password" placeholder="Password" ref="password"/>
          <input className="btn" type="submit" value="Register" />
          {this.state.error ? (
             <div className="alert">Invalid username or password.</div>
           ) : null }
        </form>
      </div>
    );
  }
});

// Run the routes
var routes = (
		<Router>
		<Route name="app" path="/" component={App}>
			<IndexRoute component = {Home}/>
			<Route name="survey" path="/survey" component={DoublePage} />
			<Route name="login" path="/login" component={Login} />
			<Route name="register" path="/register" component={Register} />
			<Route name="results"  path="/results"  component={Results} />
			<Route path="*" component={Home}/>
		</Route>
		</Router>
);

ReactDOM.render(routes, document.body);
