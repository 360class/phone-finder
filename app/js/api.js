// API object
var api = {
  // add an item, call the callback when complete
  addSurvey: function(token,ratings,preferences,cb) {
    console.log(preferences)
    var url = "/api/data";
    $.ajax({
      url: url,
      contentType: 'application/json',
      data: JSON.stringify(
      {
		      token:token,
          ratings:ratings,
          preferences:preferences,
      }),
      type: 'POST',
      headers: {},
      success: function(res) {
        if (cb)
          cb(true, res);
      },
      error: function(xhr, status, err) {
        // if there is an error, remove the login token
        //delete localStorage.token;
        if (cb)
          cb(false, status);
      }
    });
  },
  getResults: function(cb) {
    var url = "/api/results";
    $.ajax({
      url: url,
      dataType: 'json',
      type: 'GET',
      headers: {'Authorization':localStorage.token},
      success: function(res) {
        if (cb)
          cb(res);
      },
      error: function(xhr, status, err) {
        // if there is an error, remove the login token
        //delete localStorage.token;
        if (cb)
          cb(status);
      }
    });

  },
  
  // check to see if there is a user logged in already
  userLoggedIn: function() {
      return !!localStorage.token;
  },
  
  // get token from local storage
  getToken: function() {
    return localStorage.token;
  },
  
  // get name from local storage
  getName: function() {
    return localeStorage.name;
  },
  
  // login user
  login: function(username, password, cb) {
    //submit login request
    cb = arguments[arguments.length-1];
    //check for token in local storage
    if (localStorage.token) {
        this.onChange(true);
        if (cb) {
            cb(true);
        }
        return;
    }
    
    // submit request to server
    var url = "/api/login";
    $.ajax({
        url: url,
        datatype: 'json',
        type: 'POST',
        data: {
            username: username,
            password: password
        },
        success: function(res) {
            // on success, store login token
            localStorage.token = res.token;
            localStorage.name = res.name;
            this.onChange(true);
            if (cb) {
                cb(true);
            }
        }.bind(this),
        error: function(xhr, status, err) {
            // if there is an error, remove token (if any)
            delete localStorage.token;
            this.onChange(false);
            if (cb) {
                cb(false);
            }
        }.bind(this)
    });
  },
  
  // logout user
  logout: function() {
    delete localStorage.token;
    delete localStorage.name;
    this.onChange(false);
  },
  
  // register new user
  register: function(name, username, password, cb) {
    // submit register request
    var url = "api/register";
    $.ajax({
        url: url,
        datatype: 'json',
        type: 'POST',
        data: {
            name: name,
            username: username,
            password: password
        },
        // store token upon success
        success: function(res) {
            localStorage.token = res.token;
            localStorage.name = res.name;
            this.onChange(true);
            if (cb) {
                cb(true);
            }
        }.bind(this),
        error: function(xhr, status, err) {
            // if there was an error, remove token (if any)
            delete localStorage.token;
            this.onChange(false);
            if (cb) {
                cb(false);
            }
        }.bind(this)
    });
  },
  onChange: function(){ 
  },
};

window.app_api = api;
